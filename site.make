core = 7.x
api = 2

; uw_open_data
projects[uw_open_data][type] = "module"
projects[uw_open_data][download][type] = "git"
projects[uw_open_data][download][url] = "https://git.uwaterloo.ca/wcms/uw_open_data.git"
projects[uw_open_data][download][tag] = "7.x-1.2"
projects[uw_open_data][subdir] = ""